package nl.utwente.di.CelsiusToFarenheit;

public class Quoter {
    public double getFarenheit(String celsius) {
        double value = Double.parseDouble(celsius);
        double farenheit = (value*9/5) + 32;
        return farenheit;
    }
}
